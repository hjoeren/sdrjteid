package io.gitlab.hjoeren.sdrjteid;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.spi.BackendIdConverter;
import org.springframework.stereotype.Component;

@Component
public class GuardianshipIdConverter implements BackendIdConverter {

  @Autowired
  StudentRepository studentRepository;

  @Autowired
  LegalGuardianRepository legalGuardianRepository;

  @Override
  public boolean supports(Class<?> delimiter) {
    return Guardianship.class.equals(delimiter);
  }

  @Override
  public Serializable fromRequestId(String id, Class<?> entityType) {
    String[] ids = id.split("_");
    Long studentId = Long.parseLong(ids[0]);
    Student student = studentRepository.findById(studentId).orElseThrow();
    Long legalGuardianId = Long.parseLong(ids[1]);
    LegalGuardian legalGuardian = legalGuardianRepository.findById(legalGuardianId).orElseThrow();
    return new Guardianship.GuardianshipId(student, legalGuardian);
  }

  @Override
  public String toRequestId(Serializable id, Class<?> entityType) {
    Guardianship.GuardianshipId guardianshipId = (Guardianship.GuardianshipId) id;
    Long studentId = guardianshipId.getStudent().getId();
    Long legalGuardianId = guardianshipId.getLegalGuardian().getId();
    return String.format("%d_%d", studentId, legalGuardianId);
  }

}

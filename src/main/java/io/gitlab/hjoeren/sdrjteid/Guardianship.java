package io.gitlab.hjoeren.sdrjteid;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Guardianship implements Serializable {

  private static final long serialVersionUID = -4097911834895785489L;

  @EmbeddedId
  private GuardianshipId guardianshipId;
  private String name;

  @Embeddable
  @NoArgsConstructor
  @AllArgsConstructor
  @Getter
  @Setter
  @EqualsAndHashCode
  public static class GuardianshipId implements Serializable {

    private static final long serialVersionUID = 7334631735209932387L;

    @ManyToOne
    private Student student;
    @ManyToOne
    private LegalGuardian legalGuardian;

  }

}

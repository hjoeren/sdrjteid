package io.gitlab.hjoeren.sdrjteid;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Student extends AbstractPersistable<Long> {

  private String name;
  
  @OneToMany(mappedBy = "guardianshipId.student")
  private List<Guardianship> guardianships = new ArrayList<>();
  
}

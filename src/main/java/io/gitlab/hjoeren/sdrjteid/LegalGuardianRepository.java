package io.gitlab.hjoeren.sdrjteid;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LegalGuardianRepository extends JpaRepository<LegalGuardian, Long> {

}

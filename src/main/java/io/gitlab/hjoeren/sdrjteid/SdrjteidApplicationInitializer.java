package io.gitlab.hjoeren.sdrjteid;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SdrjteidApplicationInitializer {

  @Autowired
  public SdrjteidApplicationInitializer(StudentRepository studentRepository,
      LegalGuardianRepository legalGuardianRepository,
      GuardianshipRepository guardianshipRepository) {
    Student student = new Student("Hans", Collections.emptyList());
    student = studentRepository.save(student);
    LegalGuardian legalGuardian = new LegalGuardian("Peter");
    legalGuardian = legalGuardianRepository.save(legalGuardian);
    Guardianship guardianship = new Guardianship(new Guardianship.GuardianshipId(student, legalGuardian), "Cool father");
    guardianshipRepository.save(guardianship);
  }

}

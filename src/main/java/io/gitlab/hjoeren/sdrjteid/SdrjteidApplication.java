package io.gitlab.hjoeren.sdrjteid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdrjteidApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdrjteidApplication.class, args);
	}

}

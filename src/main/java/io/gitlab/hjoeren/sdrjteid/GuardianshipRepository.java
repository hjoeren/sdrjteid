package io.gitlab.hjoeren.sdrjteid;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GuardianshipRepository
    extends JpaRepository<Guardianship, Guardianship.GuardianshipId> {

}
